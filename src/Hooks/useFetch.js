
import axios from 'axios';
import { useEffect, useState } from 'react';

let source = axios.CancelToken.source();

export const useFetch = (url) => {
    const [list, setList] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        fetchUrl();

        return () => {
            source.cancel('this is stopped');
        }
    }, [])

    const fetchUrl = async () => {
        setIsLoading(true);
        setIsError(false);

        source = axios.CancelToken.source();
        let config = { cancelToken: source.token }

        try {
            const resp = await axios.get(url, config);
            setList(resp.data);
            setIsLoading(false);
        } catch (error) {
            setIsLoading(false);
            setIsError(error.message);
            console.log('ERROR', error);
        }
    }

    return {
        list,
        isLoading,
        isError
    }
}