import React from 'react';

class MyCounterClass extends React.Component {
    state = {
        count: 5,
        count2: 4
    }

    handleAdd = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        console.log('render', this.state);

        return (
            <div>
                <h1>Count, {this.state.count}</h1>
                <button onClick={this.handleAdd}>Add</button>
            </div>
        )
    }
}

export default MyCounterClass;