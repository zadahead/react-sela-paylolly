import { useDispatch } from "react-redux";
import { addCount } from "State/count";
import { Btn } from "UIKit";

const ReduxCounterAdd = () => {
    const dispatch = useDispatch();

    const handleAdd = () => {
        dispatch(addCount());
    }

    return (
        <div>
            <Btn onClick={handleAdd}>Add</Btn>
        </div>
    )
}

export default ReduxCounterAdd;