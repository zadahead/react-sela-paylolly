import { useCallback } from "react";
import { useState } from "react";
import { Btn, Line } from "UIKit";
import CallbackCounterDisplay from "./CallbackCounterDisplay";

const CallbackCounter = () => {
    const [count, setCount] = useState(0);
    const [isRed, setIsRed] = useState(false);

    console.log('render');

    const handleAdd = useCallback(() => {
        setCount(count + 1);
    }, [count])

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return (
        <div>
            <h4 style={styleCss}>Count, {count}</h4>
            <CallbackCounterDisplay count={count} handleAdd={handleAdd} />
            <Line>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleSwitch}>Switch</Btn>
            </Line>

        </div>
    )
}

export default CallbackCounter;