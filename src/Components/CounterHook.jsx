import { useCounter } from 'Hooks/useCounter';


const CounterHook = () => {
    //logic
    const { count, handleAdd } = useCounter();

    //render
    return (
        <div>
            <h4>Counter: {count}</h4>
            <button onClick={handleAdd}>Add</button>
        </div>
    )
}

export default CounterHook;