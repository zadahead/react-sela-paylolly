import { useState } from "react";

const CounterSwitcher = () => {
    const [count, setCount] = useState(0);
    const [isRed, setIsRed] = useState(false);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return (
        <div>
            <h1 style={styleCss}>Count, {count}</h1>
            <button onClick={handleAdd}>Add</button>
            <button onClick={handleSwitch}>Switch</button>
        </div>

    )
}

export default CounterSwitcher;