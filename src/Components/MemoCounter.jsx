import { useState, useMemo } from "react";
import { Btn } from "UIKit";

const MemoCounter = () => {
    const [count, setCount] = useState(0);
    const [isRed, setIsRed] = useState(false);

    const handleAdd = () => {
        console.log('press');
        setCount(count + 1);
    }

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const newCount = useMemo(() => {
        let result = 0;
        for (let i = 0; i < 1000000000; i++) {
            result++;
        }
        return result * count;
    }, [count])


    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return (
        <div>
            <h4 style={styleCss}>Count, {newCount}</h4>
            <Btn onClick={handleAdd}>Add</Btn>
            <Btn onClick={handleSwitch}>Switch</Btn>
        </div>
    )
}

export default MemoCounter;