import React from "react";
import MyElement from "./MyElement";

//Class Component
class MyClassElement extends React.Component {
    render() {
        console.log('class props', this.props);

        return <MyElement {...this.props} />
    }
}

export default MyClassElement;