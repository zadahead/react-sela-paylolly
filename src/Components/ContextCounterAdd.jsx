import { countContext } from 'Context/countContext';
import { useContext } from 'react';
import { Btn } from 'UIKit';

const ContextCounterAdd = () => {
    const context = useContext(countContext);

    return (
        <div>
            <Btn onClick={context.handleAdd}>Add</Btn>
        </div>
    )
}

export default ContextCounterAdd;