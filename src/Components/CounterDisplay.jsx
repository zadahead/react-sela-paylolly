const CounterDisplay = (props) => {
    return (
        <h5>Function Count, {props.count}</h5>
    )
}

export default CounterDisplay;