import React from "react";
import { Btn } from "UIKit";

const CallbackCounterDisplay = ({ count, handleAdd }) => {
    console.log('<CallbackCounterDisplay /> render');
    return (
        <div>
            <h4>CallbackCounterDisplay == {count}</h4>
            <Btn onClick={handleAdd}>Inner</Btn>
        </div>
    )
}

export default React.memo(CallbackCounterDisplay);