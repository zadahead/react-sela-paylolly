import { useState } from "react";

const User = ({ user }) => {
    const { id, name, age } = user;
    const [isShow, setIsShow] = useState(true);

    const handleToggle = () => {
        setIsShow(!isShow);
    }

    return (
        <div>
            <h3>
                {id}: {name} {isShow && `(${age})`}
                <button onClick={handleToggle}>{isShow ? 'Hide' : 'Show'}</button>
            </h3>
        </div>
    )
}

export default User;

