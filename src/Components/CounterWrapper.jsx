import { useState } from "react";
import CounterAdd from "./CounterAdd";
import CounterDisplay from "./CounterDisplay";

const CounterWrapper = () => {
    const [count, setCount] = useState(10);

    const handleAdd = () => {
        setCount(count + 1);
    }

    return (
        <div>
            <CounterDisplay count={count} />
            <CounterAdd setCount={handleAdd} />
        </div>
    )
}

export default CounterWrapper;