import { valueContext } from "Context/valueContext";
import { useContext } from "react";


const ValueConsumer = () => {
    const context = useContext(valueContext);
    console.log(context);

    return (
        <div>
            <h1>ValueConsumer, {context.count}</h1>
        </div>
    )
}

export default ValueConsumer;