//Functional Component
export const MyElement = (props) => {

    const handleClick = (e) => {
        props.onClick(props);
    }


    return (
        <div>
            <h1>JSX Element</h1>
            <h2>Name: {props.name}, Age: {props.age}</h2>
            <button onClick={handleClick}>Log</button>
        </div>
    )
}

export const calc = (a, b) => {
    return a + b;
}

export default MyElement;