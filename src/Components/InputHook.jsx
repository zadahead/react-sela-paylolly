import { useInput } from "Hooks/useInput";
import { Btn, Input } from "UIKit";



const InputHook = () => {
    const username = useInput('', 'username...');
    const password = useInput('', 'password...', 'password');

    const handleCheck = () => {
        console.log(username.value, password.value);
    }

    return (
        <div>
            <Input {...username} />
            <Input {...password} />
            <Btn onClick={handleCheck}>Check</Btn>
        </div>
    )
}

export default InputHook;