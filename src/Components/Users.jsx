import User from './User';

const list = [
    { id: 1, name: 'mosh', age: 25 },
    { id: 2, name: 'mosh2', age: 35 },
    { id: 3, name: 'mosh3', age: 45 },
    { id: 4, name: 'mosh4', age: 55 }
]

const Users = (props) => {
    console.log(props);

    const renderList = () => {
        return list.map(i => {
            return <User key={i.id} user={i} />;
        })
    }

    const styleCss = {
        color: 'red',
        backgroundColor: 'yellow'
    }

    return (
        <div>
            <h1 style={styleCss}>Users</h1>
            {props.children}
            <div>
                {renderList()}
            </div>

        </div>
    )
}

/*
 <Box headline="some header">
    <h2>content</h2>
    <h2>content</h2>
    <h2>content</h2>
</Box>
*/

/*
Base
<User user={user} /> => #id: #name (#age)

Advanced
#id: #name (#age) <Hide>
#id: #name <Show>
*/
export default Users;