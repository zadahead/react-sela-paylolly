import React from 'react';

class LifeCycleClass extends React.Component {

    state = {
        count: 0
    }

    componentDidMount = () => {
        console.log('componentDidMount');
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        console.log(prevProps, prevState, snapshot);
        console.log('componentDidUpdate');
    }

    componentWillUnmount = () => {
        console.log('componentWillUnmount');
    }

    handleAdd = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render = () => {
        console.log('render');
        return (
            <div>
                <h1>LifeCycleClass, {this.state.count}</h1>
                <button onClick={this.handleAdd}>Add</button>
            </div>
        )
    }
}

export default LifeCycleClass;