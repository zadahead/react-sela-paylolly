import { useSelector } from "react-redux";

const ReduxCounterDisplay = () => {
    const count = useSelector((state) => state.count);
    //console.log(count);

    return (
        <div>
            <h2>ReduxCounterDisplay, {count}</h2>
        </div>
    )
}

export default ReduxCounterDisplay;