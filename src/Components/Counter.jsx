const Counter = (props) => {

    const handleAdd = () => {
        props.setCount(props.count + 1);
    }

    return (
        <div>
            <h1>Function Count, {props.count}</h1>
            <button onClick={handleAdd}>Add</button>
        </div>
    )
}

export default Counter;