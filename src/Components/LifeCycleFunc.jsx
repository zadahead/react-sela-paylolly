import { useEffect, useState } from "react";

/*
1) did mount
2) did update
3) will unmount
*/
const LifeCycleFunc = () => {
    const [count, setCount] = useState(0);
    const [isRed, setIsRed] = useState(true);

    useEffect(() => {
        console.log('componentDidMount');

        return () => {
            console.log('componentWillUnmount');
        }
    }, [])

    useEffect(() => {
        console.log('componentDidUpdate', count);

        return () => {
            console.log('componentWillUpdate', count);
        }
    })

    useEffect(() => {
        console.log('countDidUpdate', count);
        return () => {
            console.log('countWillUpdate', count);
        }
    }, [count])

    const handleAdd = () => {
        setCount(count + 1);
    }

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return (
        <div>
            <h1 style={styleCss}>LifeCycleFunc, {count}</h1>
            <button onClick={handleAdd}>Add</button>
            <button onClick={handleSwitch}>Switch</button>
        </div>
    )
}


/*

1) first render -- "loading..."
2) componentDidMount --- "Now its live", after 2 seconds. (setTimeout)

*/

export default LifeCycleFunc;