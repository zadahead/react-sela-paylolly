
const Box = (props) => {

    const wrapperCss = {
        border: '1px solid #e1e1e1',
        margin: '50px 20px',
    }

    const headlineCss = {
        backgroundColor: '#d1d1d1',
        margin: '0',
        padding: '10px'
    }

    const contentCss = {
        padding: '10px'
    }
    return (
        <div style={wrapperCss}>
            <h2 style={headlineCss}>{props.headline}</h2>
            <div style={contentCss}>
                {props.children}
            </div>
        </div>
    )
}

export default Box;