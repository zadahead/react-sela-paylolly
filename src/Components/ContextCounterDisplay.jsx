import { countContext } from "Context/countContext";
import { useContext } from "react";

const ContextCounterDisplay = () => {
    const context = useContext(countContext);
    return (
        <div>
            <h2>Count, {context.count}</h2>
        </div>
    )
}

export default ContextCounterDisplay;