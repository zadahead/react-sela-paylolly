import { useState } from 'react';

const Toggler = (props) => {
    const [isDisplay, setIsDisplay] = useState(true);

    const handleToggle = () => {
        setIsDisplay(!isDisplay);
    }

    return (
        <div>
            <h1>Toggler</h1>
            <button onClick={handleToggle}>Toggle</button>

            {isDisplay && props.element}
        </div>
    )
}

export default Toggler;