import { useState } from 'react';
import { Btn, Input } from 'UIKit';

const LoginWrap = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleUsernameChange = (e) => {
        setUsername(e.target.value);
    }

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    }

    const handleLogin = () => {
        console.log(username, password);
    }

    return (
        <div>
            <Input placeholder="username.." value={username} onChange={handleUsernameChange} />
            <Input placeholder="password.." type="password" value={password} onChange={handlePasswordChange} />
            <Btn onClick={handleLogin}>Login</Btn>
        </div>
    )
}

export default LoginWrap;