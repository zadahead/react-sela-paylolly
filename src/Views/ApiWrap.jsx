import Box from "Components/Box";
import { useFetch } from "Hooks/useFetch";


const styleCss = {
    color: '#e1e1e1'
}


const ApiWrap = () => {
    //logic
    const { list, isLoading, isError } = useFetch('https://jsonplaceholder.typicode.com/todos');

    //render
    const renderList = () => {
        return list.map(i => (
            <h4 key={i.id} style={i.completed ? styleCss : {}}>{i.title}</h4>
        ))
    }

    const renderContent = () => {
        if (isLoading) {
            return <h4>loading..</h4>;
        }
        if (isError) {
            return <h4 style={{ color: 'red' }}>{isError}</h4>;
        }
        return renderList();
    }

    return (
        <div>
            <Box headline="API Wrap">
                {renderContent()}
            </Box>
        </div>
    )
}

export default ApiWrap;