import Box from 'Components/Box';
import { useInput } from 'Hooks/useInput';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { deleteTodos, getTodos, patchTodos, postTodos } from 'State/todos';
import { Btn, Checkbox, Icon, Input, Line } from 'UIKit';

const styleCompleted = {
    color: '#e1e1e1',
    textDecoration: 'line-through'
}

const TodosView = () => {
    const newTodo = useInput();

    const list = useSelector((state) => state.todos);
    const dispatch = useDispatch();

    console.log('list', list);

    //CRUD

    //Read (GET)
    useEffect(() => {
        dispatch(getTodos());
    }, [])

    //Update (PATCH)
    const handlePatch = (item) => {
        dispatch(patchTodos(item.id));
    }

    //Delete (DELETE)
    const handleDelete = (item) => {
        dispatch(deleteTodos(item.id));
    }

    //Add (POST)
    const handleAdd = () => {
        dispatch(postTodos(newTodo.value));
    }

    return (
        <div>
            <Box headline="Todos View">
                {list.map(i => (
                    <div key={i.id}>
                        <Line Between>
                            <Line>
                                <Checkbox checked={i.completed} onChange={() => handlePatch(i)} />
                                <h3 style={i.completed ? styleCompleted : {}}>{i.title}</h3>
                            </Line>
                            <Icon i="trash" onClick={() => handleDelete(i)} />
                        </Line>
                    </div>
                ))}
                <Line>
                    <Input {...newTodo} />
                    <Btn onClick={handleAdd}>Add</Btn>
                </Line>
            </Box>
        </div>
    )
}

export default TodosView;