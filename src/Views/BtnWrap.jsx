import { useState } from "react";
import { Btn, Checkbox, Line } from "UIKit";

const ButtonWrap = () => {
    const [checked, setChecked] = useState(true);

    const handleChecked = () => {
        setChecked(!checked);
    }

    const handleClick = () => {
        console.log('CLICK');
        console.log(checked);
    }

    return (
        <div>
            <Line Rows>
                <Checkbox checked={checked} onChange={handleChecked}>Check A </Checkbox>
                <Btn onClick={handleClick}>Click Me</Btn>
            </Line>
        </div>
    )
}

export default ButtonWrap;