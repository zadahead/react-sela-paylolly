import { useEffect } from "react";
import { useRef } from "react";
import { Input } from "UIKit";

const Refs = () => {
    const inputRef = useRef();

    useEffect(() => {
        const input = inputRef.current;
        input.setAttribute('style', 'color: red');
    }, [])

    return (
        <div>
            <Input ref={inputRef} defaultValue="asdasdasd" />
        </div>
    )
}

export default Refs;