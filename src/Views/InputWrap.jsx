import { useState } from "react";
import { Input } from "UIKit";

const InputWrap = () => {
    const [value, setValue] = useState('');

    const handleChange = (e) => {
        setValue(e.target.value);
    }

    console.log('render:', value);

    return (
        <div>
            <h1>Input Wrap</h1>
            <Input value={value} onChange={handleChange} placeholder="username" />
        </div>
    )
}

export default InputWrap;