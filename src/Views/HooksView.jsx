import Box from 'Components/Box';
import CallbackCounter from 'Components/CallbackCounter';
import ContextCounterDisplay from 'Components/ContextCounterDisplay';
import CounterHook from 'Components/CounterHook';
import InputHook from 'Components/InputHook';
import MemoCounter from 'Components/MemoCounter';
import ReduxCounterAdd from 'Components/ReduxCounterAdd';
import ReduxCounterDisplay from 'Components/ReduxCounterDisplay';

/*
    useState
    useEffect
    useRef

    customHooks
*/

const HooksView = () => {
    return (
        <div>
            <Box headline="useCallback (+React.memo)">
                <CallbackCounter />
            </Box>

            <Box headline="useMemo">
                <MemoCounter />
            </Box>
            <Box headline="Redux">
                <ReduxCounterDisplay />
                <ReduxCounterAdd />
            </Box>
            <Box headline="Context">
                <ContextCounterDisplay />
            </Box>
            <Box headline="Input Hook">
                <InputHook />
            </Box>
            <Box headline="Counter Hook">
                <CounterHook />
            </Box>
        </div>
    )
}

export default HooksView;