import Box from "Components/Box";
import { useState } from "react";
import { Dropdown } from "UIKit";

const list = [
    { id: 1, value: 'item 1' },
    { id: 2, value: 'item 2' },
    { id: 3, value: 'item 3' },
    { id: 4, value: 'item 4' }
]

const DropDownView = () => {
    const [selected, setSelected] = useState();

    return (
        <div>
            <Box headline="Dropdown view">
                <Dropdown list={list} selected={selected} onChange={setSelected} />
            </Box>
        </div>
    )
}

export default DropDownView;