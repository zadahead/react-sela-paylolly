import './Line.css';

export const Line = ({ children, ...settings }) => {
    const clx = Object.keys(settings).join(' ');

    return (
        <div className={`Line ${clx}`}>
            {children}
        </div>
    )
}

export default Line;