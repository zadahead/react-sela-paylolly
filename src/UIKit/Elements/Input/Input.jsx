import React from 'react';
import './Input.css';

const InputComp = (props, ref) => {
    return (
        <div className='Input'>
            <input {...props} ref={ref} />
        </div>
    )
}

export const Input = React.forwardRef(InputComp);

