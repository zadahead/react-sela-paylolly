import { useRef } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { Line } from 'UIKit';
import Icon from '../Icon/Icon';
import './Dropdown.css';

export const Dropdown = ({ list, selected, onChange }) => {
    const [isOpen, setIsOpen] = useState(false);
    const wrapperRef = useRef();

    useEffect(() => {
        document.body.addEventListener('click', handleBodyClick);

        return () => {
            document.body.removeEventListener('click', handleBodyClick);
        }
    }, [])

    const handleBodyClick = (e) => {
        if (!wrapperRef.current) { return false }
        if (!wrapperRef.current.contains(e.target)) {
            setIsOpen(false);
        }
    }

    const handleToggle = (e) => {
        //e.stopPropagation();
        setIsOpen(!isOpen);
    }

    const handleSelect = (item) => {
        onChange(item.id);
        setIsOpen(false);
    }

    const getSelected = () => {
        return list.find(i => i.id === selected);
    }

    const renderTitle = () => {
        const sel = getSelected();
        if (sel) {
            return sel.value;
        }
        return 'Please Select';
    }


    return (
        <div className='Dropdown' ref={wrapperRef}>
            <div className='trig' onClick={handleToggle}>
                <Line Between>
                    <h3>{renderTitle()}</h3>
                    <Icon i="chevron-down" />
                </Line>
            </div>
            {
                isOpen && (
                    <div className='list'>
                        {list.map(i => (
                            <h4
                                key={i.id}
                                className={`${i.id === selected ? 'selected' : ''}`}
                                onClick={() => handleSelect(i)}
                            >
                                {i.value}
                            </h4>
                        ))}
                    </div>
                )
            }
        </div>
    )
}