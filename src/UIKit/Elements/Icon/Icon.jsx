export const Icon = (props) => {
    return (
        <div onClick={props.onClick}>
            <i className={`fas fa-${props.i}`}></i>
        </div>
    )
}

export default Icon;