import { Line, Icon } from 'UIKit';

import './Checkbox.css';

export const Checkbox = ({ children, checked, onChange }) => {


    return (
        <div className='Checkbox' onClick={onChange}>
            <Line>
                <Icon i={checked ? `check-square` : `square`} />
                {children}
            </Line>
        </div>
    )
}

export default Checkbox;