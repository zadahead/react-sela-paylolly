import { Line, Icon } from 'UIKit';

import './Btn.css';

export const Btn = (props) => {
    return (
        <button onClick={props.onClick} className='Btn'>
            <Line>
                {props.children}
                {props.i && <Icon i={props.i} />}
            </Line>
        </button>
    )
}

export default Btn;