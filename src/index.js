import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

//react router
import { BrowserRouter as Router } from 'react-router-dom';

//Redux
import { Provider } from "react-redux";
import store from "State/store";


ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);



