
//Actions
export const addCount = () => {
    return {
        type: 'ADD_COUNT',
        payload: 1
    }
}

//Reducers 

export const countReducer = (count = 10, action) => {
    switch (action.type) {
        case 'ADD_COUNT': return count + action.payload;
        default: return count;
    }
}