import { createStore, combineReducers, applyMiddleware } from "redux";

import { countReducer } from './count';
import { getMiddleTodos, todosReducer } from "./todos";

//thunk
import ReduxThunk from "redux-thunk";

const reducers = combineReducers({
    count: countReducer,
    todos: todosReducer
})

const middlewares = applyMiddleware(getMiddleTodos, ReduxThunk);

const store = createStore(reducers, middlewares);

export default store;
