import axios from "axios";

//Actions
export const getTodosPrev = () => {
    return {
        type: 'GET_TODOS',
        payload: [{ id: 1, title: 'asadas' }]
    }
}

export const getTodos = () => {
    return async (dispatch) => {
        const resp = await axios.get('https://jsonplaceholder.typicode.com/todos');
        //waiting....
        dispatch({
            type: 'GET_TODOS',
            payload: resp.data.splice(0, 8)
        })
    }
}

export const patchTodos = (id) => {
    return {
        type: 'PATCH_TODOS',
        payload: id
    }
}

export const deleteTodos = (id) => {
    return {
        type: 'DELETE_TODOS',
        payload: id
    }
}

export const postTodos = (title) => {
    return {
        type: 'POST_TODOS',
        payload: title
    }
}

//dispatch, next, action

export const getMiddleTodos = ({ dispatch }) => {
    return (next) => {
        return (action) => {
            next(action);
        }
    }
}


//Reducers

export const todosReducer = (list = [], action) => {
    switch (action.type) {
        case 'GET_TODOS': return action.payload;
        case 'PATCH_TODOS': {
            const newList = [...list];
            const index = newList.findIndex(i => i.id === action.payload);
            newList[index].completed = !newList[index].completed;

            return newList;
        }
        case 'DELETE_TODOS': {
            const newList = [...list];
            const index = newList.findIndex(i => i.id === action.payload);
            newList.splice(index, 1);

            return newList;
        }
        case 'POST_TODOS': {
            const newList = [...list, {
                id: new Date().toJSON(),
                title: action.payload,
                completed: false
            }];

            return newList;
        }
        default: return list;
    }
}