import { useState } from "react";
import { countContext } from "./countContext";

export const CountProvider = ({ children }) => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    return (
        <countContext.Provider value={{ count, handleAdd }}>
            {children}
        </countContext.Provider>
    )
}