
import { Line, Grid } from 'UIKit';
import LoginWrap from 'Views/Login';
import './App.css';

import { Routes, Route, NavLink } from 'react-router-dom';
import UserWrap from 'Views/UserWrap';
import Refs from 'Views/Refs';
import DropDownView from 'Views/DropdownView';
import HooksView from 'Views/HooksView';
import ApiWrap from 'Views/ApiWrap';

//context 
import ContextCounterAdd from 'Components/ContextCounterAdd';
import { CountProvider } from 'Context/CountProvider';
import TodosView from 'Views/TodosView';



const App = () => {


    return (
        <CountProvider>
            <div className='App'>
                <Grid>
                    <div>
                        <Line Between>
                            <Line>
                                <div>logo</div>
                            </Line>
                            <Line>
                                <NavLink to="/login">login</NavLink>
                                <NavLink to="/refs">refs</NavLink>
                                <NavLink to="/user/asdasdasd">user</NavLink>
                                <NavLink to="/dropdown">dropdown</NavLink>
                                <NavLink to="/hooks">hooks</NavLink>
                                <NavLink to="/api">api</NavLink>
                                <NavLink to="/todos">todos</NavLink>
                            </Line>
                            <div>
                                <ContextCounterAdd />
                            </div>
                        </Line>
                    </div>
                    <div>
                        <Routes>
                            <Route path='/login' element={<LoginWrap />} />
                            <Route path='/refs' element={
                                <div>
                                    <Refs />
                                    <Refs />
                                    <Refs />
                                </div>
                            } />

                            <Route path='/test' element={<h4>Test</h4>} />
                            <Route path='/test/:id' element={<h4>Test with an ID</h4>} />
                            <Route path='/mosh' element={<h4>Test Mosh</h4>} />
                            <Route path='/mosh/*' element={<h4>Test Mosh rest</h4>} />

                            <Route path='/user/:mosh' element={<UserWrap />} />
                            <Route path='/dropdown' element={<DropDownView />} />
                            <Route path='/hooks' element={<HooksView />} />
                            <Route path='/api' element={<ApiWrap />} />
                            <Route path='/todos' element={<TodosView />} />
                        </Routes>
                    </div>
                </Grid>
            </div>
        </CountProvider>
    )
}

/*

*/

export default App;